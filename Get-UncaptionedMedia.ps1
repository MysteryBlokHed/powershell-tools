function Get-UncaptionedMedia {
    <#
        .SYNOPSIS
        List uncaptioned video files in the current directory.

        .DESCRIPTION
        Iterates over folders and checks if a subtitle (.srt) file exists for each video file.  

        .PARAMETER Path
        The path to search. Defaults to the current directory.

        .PARAMETER Recurse
        Recursively check folders.

        .NOTES
        Written by Adam Thompson-Sharpe.
        Licensed under either of the Apache License, Version 2.0,
        or the MIT license, at your option.

        Source: <https://gitlab.com/MysteryBlokHed/powershell-tools>
    #>
    param(
        [Parameter(
            Position = 0,
            ValueFromPipeline = $True
        )]
        $Path = (Get-Location),
        [switch]$Recurse
    )

    $Path = Get-Item $Path

    $VideoExtensions = '264', '265', 'asf', 'avc', 'avi', 'divx', 'flv', 'h264', 'h265', 'hevc', 'm2ts', 'm2v', 'm4v', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'mpv', 'mts', 'rar', 'ts', 'vob', 'webm', 'wmv'
    $VideoExtensionsFilter = $VideoExtensions | ForEach-Object { "*.$_" }

    $Folders = Get-ChildItem -Path $Path -Directory -Recurse:$Recurse
    Write-Debug "Checking folders $Folders"
    $Start = $Path
    Write-Debug "Starting in $Start"

    foreach ($Folder in $Folders) {
        Write-Debug "Checking $($Folder.FullName)"
        Set-Location $Folder.FullName

        $Media = Get-ChildItem -Path ".\*" -File -Include $VideoExtensionsFilter

        foreach ($Item in $Media) {
            $Subtitles = Get-Item -Path "$($Item.BaseName).srt" -ErrorAction SilentlyContinue
            If (-not $Subtitles) {
                Write-Output "$($Item.FullName) has no captions!"
            }
        }

        Set-Location $Start
    }
}
