function Invoke-MRMerge {
    <#
        .SYNOPSIS
        Merge a GitLab merge request with a predictable message.

        .DESCRIPTION
        Pass information about the merge such as the number and the MR title
        to create a merge message.

        .PARAMETER MRNumber
        The number of the merge request including the `!` (eg. !1 or !13)

        .PARAMETER Title
        The title of the merge request (eg. Fix: Do something)

        .PARAMETER Branch
        The branch to merge

        .NOTES
        Written by Adam Thompson-Sharpe.
        Licensed under either of the Apache License, Version 2.0,
        or the MIT license, at your option.

        Source: <https://gitlab.com/MysteryBlokHed/powershell-tools>
    #>
    param(
        [Parameter(Mandatory = $True, Position = 0)]
        [string]$MRNumber,
        [Parameter(Mandatory = $True, Position = 1)]
        [string]$Title,
        [Parameter(Mandatory = $True, Position = 2)]
        [string]$Branch
    )

    $Message = "Merge $MRNumber`: $Title"
    git merge "$Branch" --no-ff --message "$Message" --edit
}
