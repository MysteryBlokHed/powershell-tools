function Get-ReusedVar {
    <#
        .SYNOPSIS
        Prompt the user for a variable while remembering the last value.

        .DESCRIPTION
        Provide a prompt to show to the user as well as the name of the variable to save to.
        If the user provides a value, update the variable.
        If the user provides nothing, use the existing value.
        Be careful not to use the names of variables created inside this function for the -Name parameter.

        .PARAMETER Prompt
        The prompt to show the user.

        .PARAMETER Name
        The name of the variable to check/update.
        The variable with this name is automatically modified based on the user's response.

        .PARAMETER ReturnValue
        Return the variable's value instead of returning nothing.

        .NOTES
        Written by Adam Thompson-Sharpe.
        Licensed under either of the Apache License, Version 2.0,
        or the MIT license, at your option.

        Source: <https://gitlab.com/MysteryBlokHed/powershell-tools>
    #>
    param(
        [Parameter(Mandatory = $True, Position = 1)]
        [string]$Prompt,
        [Parameter(Mandatory = $True, Position = 2)]
        [string]$Name,
        [switch]$ReturnValue
    )

    $Current = Get-Variable $Name -ValueOnly -ErrorAction SilentlyContinue
    if ($Current) {
        $Prompt += " [$Current]"
    }

    while ($True) {
        $Value = Read-Host -Prompt $Prompt

        if ($Value) {
            Set-Variable $Name $Value -Visibility Public -Scope Global
            break
        }
        else {
            if (-not $Current) {
                Write-Output 'A value must be provided!'
            }
            else {
                break
            }
        }
    }

    if ($ReturnValue) { return Get-Variable $Name -ValueOnly }
}
